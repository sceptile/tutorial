from django import forms

class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField()

class PostForm(forms.Form):
	title = forms.CharField()
	body = forms.CharField()