from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.template import Template
from forms import *
from rest_framework.views import APIView
from authentication.serializers import BlogPostSerializer
from rest_framework import status
from models import *
import datetime
import requests

class JSONResponse(HttpResponse):
	"""
	An HttpResponse that renders its content into JSON.
	"""
	def __init__(self, data, **kwargs):
		content = JSONRenderer().render(data)
		kwargs['content_type'] = 'application/json'
		super(JSONResponse, self).__init__(content, **kwargs)

class post(object):
	def __init__(self, title, body, timestamp):
		self.title = title
		self.body = body
		self.timestamp = timestamp

def Login(request):
	try:
		next = request.GET['next']
	except KeyError:
		next = "/auth/login-check/"
	form = LoginForm()
	return render(request, "login.html", {'form':form, 'next':next})

	
def login_check(request):
	user = authenticate(username = request.POST['username'], password = request.POST['password'])
	if user is not None:
		if user.is_active:
			login(request, user)
			request.session['user'] = request.POST['username']
			return HttpResponseRedirect("/auth/profile/?edit")
		else:
			return HttpResponse("Inactive account")
	else:
		return HttpResponse("Either the username or the password is invalid")

# @login_required
# If POST, show only profile. If GET, allow entering new blog post
def profile(request):
	if request.method == "GET":
		posts = []
		p = BlogPost.objects.all()
		for i in p:
			posts.append(i)
		return render(request, "profile.html", {'posts':posts, 'form':""})
	else:
		form = PostForm()
		return render(request, "profile.html", {'form':form})

@login_required
@api_view(['GET', 'POST'])
def posts(request):
	if request.method == "GET":
		posts = BlogPost.objects.all()
		serializer = BlogPostSerializer(posts, many = True)
		return Response(serializer.data)
	else:
		serializer = BlogPostSerializer(data = request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def add(request):
	try:
		p = Post(title = request.POST['title'], body = request.POST['body'], timestamp = datetime.datetime.now()).save()
		return HttpResponseRedirect ("/auth/profile/?edit")
	except KeyError:
		URL = 'http://localhost:8000/auth/profile/'
		client = requests.session()
		client.get(URL)  # sets cookie
		#csrftoken = client.cookies['csrf']
		csrftoken = request.COOKIES['csrftoken']
		post_data = {'name': 'Gladys', 'csrfmiddlewaretoken':csrftoken}
		response = requests.post(URL, data=post_data)
		return HttpResponse (response.content)

def Logout (request):
	logout(request)
	return HttpResponseRedirect("/auth/login")