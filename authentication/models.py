from django.db import models
from django.contrib import admin

class BlogPost(models.Model):
	title = models.CharField(max_length = 30)
	body = models.CharField(max_length = 300)
	timestamp = models.DateTimeField()

admin.site.register(BlogPost)