from django.conf.urls import url
from authentication import views

urlpatterns = (
	url(r'^login/', views.Login),

	url(r'^login-check/', views.login_check),

	url(r'^profile/', views.profile),

	url(r'^add/', views.add),

	url(r'^posts/', views.posts),

	url(r'^logout/', views.Logout),
)